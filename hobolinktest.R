# Testing API with httr
library(httr)

# Import the username, password from a file not in version control
# Consider switching to json to be python compatible
# auth.R supplies:
# user <- "username"
# password <- "password"
# htoken <- "Onset Supplied token" 
#If you don't have a token you have to request if from Hobolink tech support
# queries <- c(StationName1="SerialNumber - Over the last week"
#              ,StationName2="SerialNumber - Over the last week"
# You can find the queries by logging in to your account https://hobolink.com/
source("auth.R")

# Now to connect to Onset
# Set the basic url for now use the dev server
rooturl <- "https://webservice.hobolink.com/restv2"

# RX3000 can only export pre-configured queries via the web tool
queryurl <- paste0(rooturl,"/data/custom/file")

#Construct the body portion of the request
q <- queries[1]
auth <- list(password=password,user=user,token=htoken)
jsonbody <- list(query=q,authentication=auth)

# Request is a POST request with details in the body as json
response <- POST(queryurl,encode="json",body=jsonbody,accept("text/csv"),content_type("application/json"))

# Save to a csv real file, parsed isn't recommended long term, what to use instead of readr read_csv
results <- content(response,"parsed",type="text/csv", encoding="UTF-8")

# A place to put the data)
dir.create("data/")
write.csv(results,"data/station1.csv")